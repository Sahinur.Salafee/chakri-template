'use strict';

jQuery(window).on('scroll', function(){
      if(jQuery(window).scrollTop()){
        jQuery('header').addClass('fixed-it')
      }
      else{
        jQuery('header').removeClass('fixed-it')
      }
    })

    $(".navbar-toggler").on('click', function() {
        $(".content-pt").toggleClass("blur-it");
        $("header").toggleClass("opacity-it");
    });

// Get the elements with class="column"
// var elements = document.getElementsByClassName("feature-job-items");
// var element_two = document.getElementsByClassName("feature-job-items-sidebar");
// Declare a loop variable
// var i;




// List View
// function listView() {
//   for (i = 0; i < elements.length; i++) {
//     elements[i].style.width = "100%";
//   }
// }

// function listShow() {
//   for (i = 0; i < element_two.length; i++) {
//     element_two[i].style.width = "100%";
//   }
// }





// //Grid View
// function gridView() {
//   for (i = 0; i < elements.length; i++) {
//     elements[i].style.width = "32.4%";
//   }
// }

// function gridShow() {
//   for (i = 0; i < element_two.length; i++) {
//     element_two[i].style.width = "48.63%";
//   }
// }





/* Optional: Add active class to the current button (highlight it) */
var container = document.getElementsByClassName("buttons-container");
var btns = document.getElementsByClassName("view-btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    if (current.length > 0) { 
    current[0].className = current[0].className.replace(" active", "");
  }
  this.className += " active";
  });
}


// $(".grid-view").click(function(){
//   $("article").addClass("grid-view-items")
// });


// $(".list-view").click(function(){
//   $("article").removeClass("grid-view-items")
// });


// $(document).ready(function(){
//     $('[data-toggle="tooltip"]').tooltip();   
// });


$(window).on('load', function() {

  $(".testimonial-slider").slick({
    dots: false,
    arrows:true,
    infinite: true,
    autoplay:false,
    centerMode:true,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: false,
    fade: true,
    cssEase: 'linear'
  });

});